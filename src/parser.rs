use std::{error::Error, fmt::Debug, collections::HashMap};

use serde::{Deserialize, Serialize, Deserializer, de::Unexpected};
use serde_aux::prelude::deserialize_number_from_string;

pub(crate) fn parse_response(input_string: &str) -> Result<MarketResponse, Box<dyn Error>> {
    Ok(serde_json::from_str(input_string)?)
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MarketResponse {
    success: bool,
    start: u32,
    pagesize: u16,
    total_count: u16,

    // The response has the "listingid" as the key. However, I use the "assetId" to make the lookup simpler
    #[serde(deserialize_with = "nested_listings_deserializer")]
    listinginfo: ListingMappipng,

    #[serde(deserialize_with = "nested_assets_deserializer")]
    assets: Vec<Asset>,

    #[serde(deserialize_with = "app_data_deserializer")]
    app_data: AppData,
}

#[allow(dead_code)]
impl MarketResponse {
    pub fn success(&self) -> bool {self.success}
    pub fn start(&self) -> u32 {self.start}
    pub fn pagesize(&self) -> u16 {self.pagesize}
    pub fn total_count(&self) -> u16 {self.total_count}
    pub fn listinginfo(&self) -> &ListingMappipng {&self.listinginfo}
    pub fn assets(&self) -> &Vec<Asset> {&self.assets}
    pub fn app_data(&self) -> &AppData {&self.app_data}
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AppData {
    appid: u32,
    name: String,
    icon: String,
    link: String,
}

type ListingMappipng = HashMap<String, ListingInfo>;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListingInfo {
    listingid: String,
    price: u32,
    fee: u32,
    publisher_fee_app: u32,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    publisher_fee_percent: f64,
    currencyid: u32,
    steam_fee: u32,
    publisher_fee: u32,
    converted_price: u32,
    converted_fee: u32,
    converted_currencyid: u32,
    converted_steam_fee: u32,
    converted_publisher_fee: u32,
    converted_price_per_unit: u32,
    converted_fee_per_unit: u32,
    converted_steam_fee_per_unit: u32,
    converted_publisher_fee_per_unit: u32,
    asset: ListingAsset,
}

impl ListingInfo {
    pub fn calculate_final_price(&self) -> f32 {
        (self.converted_price + self.converted_fee) as f32 * 0.01f32
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListingAsset {
    id: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Asset {
    currency: u8,
    appid: u32,
    contextid: String,
    id: String,
    classid: String,
    instanceid: String,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    amount: u16,
    status: u8,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    original_amount: u16,
    unowned_id: String,
    unowned_contextid: String,
    background_color: String,
    // icon_url: String, // It's not a URL and is not relevant, so IDC
    icon_url_large: String, // It's not a URL and is not relevant, so IDC
    descriptions: Vec<DescriptionFragment>,
    #[serde(deserialize_with = "bool_from_int")]
    tradable: bool,
    actions: Vec<Action>,
    #[serde(default)]
    fraudwarnings: Vec<String>,
    name: String,
    name_color: String,
    #[serde(rename = "type")]
    kind: String,
    market_name: String,
    market_hash_name: String,
    market_actions: Vec<Action>,
    #[serde(deserialize_with = "bool_from_int")]
    commodity: bool,
    market_tradable_restriction: u16,
    #[serde(deserialize_with = "bool_from_int")]
    marketable: bool,
    app_icon: String,
    owner: u32,
}

#[allow(dead_code)]
impl Asset {
    pub fn currency(&self) -> u8 {self.currency}
    pub fn appid(&self) -> u32 {self.appid}
    pub fn contextid(&self) -> &str {&self.contextid}
    pub fn id(&self) -> &str {&self.id}
    pub fn classid(&self) -> &str {&self.classid}
    pub fn instanceid(&self) -> &str {&self.instanceid}
    pub fn amount(&self) -> u16 {self.amount}
    pub fn status(&self) -> u8 {self.status}
    pub fn original_amount(&self) -> u16 {self.original_amount}
    pub fn unowned_id(&self) -> &str {&self.unowned_id}
    pub fn unowned_contextid(&self) -> &str {&self.unowned_contextid}
    pub fn background_color(&self) -> &str {&self.background_color}
    pub fn descriptions(&self) -> &Vec<DescriptionFragment> {&self.descriptions}
    pub fn tradable(&self) -> bool {self.tradable}
    pub fn actions(&self) -> &Vec<Action> {&self.actions}
    pub fn fraudwarnings(&self) -> &Vec<String> {&self.fraudwarnings}
    pub fn name(&self) -> &str {&self.name}
    pub fn name_color(&self) -> &str {&self.name_color}
    pub fn kind(&self) -> &str {&self.kind}
    pub fn market_name(&self) -> &str {&self.market_name}
    pub fn market_hash_name(&self) -> &str {&self.market_hash_name}
    pub fn market_actions(&self) -> &Vec<Action> {&self.market_actions}
    pub fn commodity(&self) -> bool {self.commodity}
    pub fn market_tradable_restriction(&self) -> u16 {self.market_tradable_restriction}
    pub fn marketable(&self) -> bool {self.marketable}
    pub fn app_icon(&self) -> &str {&self.app_icon}
    pub fn owner(&self) -> u32 {self.owner}

    pub fn has_nametag(&self) -> bool {
        for warning in self.fraudwarnings() {
            if warning.starts_with("Namensschild:") {
                return true;
            }
        }

        false
    }

    pub fn get_nametag(&self) -> Option<String> {
        for warning in self.fraudwarnings() {
            if warning.starts_with("Namensschild:") {
                return Some(warning.strip_prefix("Namensschild: ''").unwrap().strip_suffix("''").unwrap().to_string());
            }
        }

        None
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DescriptionFragment {
    #[serde(rename = "type")]
    kind: String,
    value: String,
    color: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Action {
    link: String,
    name: String,
}

fn bool_from_int<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    match u8::deserialize(deserializer)? {
        0 => Ok(false),
        1 => Ok(true),
        other => Err(serde::de::Error::invalid_value(
            Unexpected::Unsigned(other as u64),
            &"zero or one",
        )),
    }
}

fn app_data_deserializer<'de, D>(deserializer: D) -> Result<AppData, D::Error>
where
    D: Deserializer<'de>,
{
    #[derive(Deserialize)]
    struct AppDataWithClutter {
        #[serde(rename = "730")]
        data: AppData
    }

    Ok(AppDataWithClutter::deserialize(deserializer)?.data)
}

fn nested_listings_deserializer<'de, D>(deserializer: D) -> Result<HashMap<String, ListingInfo>, D::Error>
where
    D: Deserializer<'de>,
{
    type ListingsWithId = HashMap<String, ListingInfo>;

    Ok(ListingsWithId::deserialize(deserializer)?.into_values()
        .map(|listing| (listing.asset.id.to_string(), listing))
        .collect()
    )
}

fn nested_assets_deserializer<'de, D>(deserializer: D) -> Result<Vec<Asset>, D::Error>
where
    D: Deserializer<'de>,
{
    #[derive(Deserialize)]
    struct AssetsWithTwo {
        #[serde(rename = "2")]
        data: HashMap<String, Asset>
    }

    #[derive(Deserialize)]
    struct AssetsWithAppId {
        #[serde(rename = "730")]
        data: AssetsWithTwo
    }

    Ok(AssetsWithAppId::deserialize(deserializer)?.data.data.into_values().collect())
}
