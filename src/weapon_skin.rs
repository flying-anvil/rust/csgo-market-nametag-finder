use core::panic;
use std::{fmt::Display, str::FromStr, error::Error};

use rand::Rng;

use crate::weapon_search::WeaponName;

pub fn random_skin_for_weapon(weapon: &WeaponName) -> WeaponSkin {
    let skins = get_known_skins_for_weapon(weapon);

    if skins.is_empty() {
        panic!("Weapon {} has no known skins yet", weapon.as_str());
    }

    let index = rand::thread_rng().gen_range(0..skins.len());

    skins[index].clone()
}

pub fn get_known_skins_for_weapon(weapon: &WeaponName) -> Vec<WeaponSkin> {
    match weapon {
        WeaponName::Knife => vec![

        ],

        // Pistol
        WeaponName::CZ75Auto => vec![

        ],
        WeaponName::DesertEagle => vec![

        ],
        WeaponName::DualBerettas => vec![

        ],
        WeaponName::FiveSeveN => vec![

        ],
        WeaponName::Glock18 => vec![

        ],
        WeaponName::P2000 => vec![

        ],
        WeaponName::P250 => vec![

        ],
        WeaponName::R8Revolver => vec![

        ],
        WeaponName::Tec9 => vec![

        ],
        WeaponName::USPS => vec![

        ],

        // Shotgun
        WeaponName::MAG7 => vec![

        ],
        WeaponName::Nova => vec![

        ],
        WeaponName::SawedOff => vec![

        ],
        WeaponName::XM1014 => vec![

        ],

        // Heavy
        WeaponName::M249 => vec![

        ],
        WeaponName::Negev => vec![

        ],

        // MP
        WeaponName::MAC10 => vec![

        ],
        WeaponName::MP5SD => vec![

        ],
        WeaponName::MP7 => vec![

        ],
        WeaponName::MP9 => vec![

        ],
        WeaponName::P90 => vec![

        ],
        WeaponName::PPBizon => vec![

        ],
        WeaponName::UMP45 => vec![

        ],

        // Rifle
        WeaponName::AK47 => vec![

        ],
        WeaponName::AUG => vec![

        ],
        WeaponName::FAMAS => vec![

        ],
        WeaponName::GalilAR => vec![

        ],
        WeaponName::M4A1S => vec![
            WeaponSkin::Flashback,
            WeaponSkin::NightTerror,
            WeaponSkin::EmphorosaurS,
            WeaponSkin::Briefing,
            WeaponSkin::BloodTiger,
            WeaponSkin::LeadedGlass,
            WeaponSkin::Basilisk,
            WeaponSkin::Nightmare,
            WeaponSkin::Decimator,
            WeaponSkin::AtomicAlloy,
            WeaponSkin::PlayerTwo,
            WeaponSkin::ChanticosFire,
            WeaponSkin::ChanticosFire,
            WeaponSkin::Cyrex,
            WeaponSkin::MechaIndustries,
            WeaponSkin::MechaIndustries,
            WeaponSkin::HyperBeast,
            WeaponSkin::Guardian,
            WeaponSkin::BrightWater,
            WeaponSkin::GoldenCoil,
            WeaponSkin::DarkWater,
            WeaponSkin::Printstream,
        ],
        WeaponName::M4A4 => vec![

        ],

        // Sniper
        WeaponName::SG553 => vec![

        ],
        WeaponName::AWP => vec![

        ],
        WeaponName::G3SG1 => vec![

        ],
        WeaponName::SCAR20 => vec![

        ],
        WeaponName::SSG08 => vec![

        ],
    }
}

// see https://counterstrike.fandom.com/wiki/Skins/List
#[derive(Clone, Debug)]
pub enum WeaponSkin {
    // Knife

    // Pistol
      // CZ75Auto
      // DesertEagle
      // DualBerettas
      // FiveSeveN
      // Glock18
      // P2000
      // P250
      // R8Revolver
      // Tec9
      // USPS

    // Shotgun
      // MAG7
      // Nova
      // SawedOff
      // XM1014

    // Heavy
      // M249
      // Negev

    // MP
      // MAC10
      // MP5SD
      // MP7
      // MP9
      // P90
      // PPBizon
      // UMP45

    // Rifle
      // AK47
      // AUG
      // FAMAS
      // GalilAR
      // M4A1S
      Flashback,
      Briefing,
      NightTerror,
      EmphorosaurS,
      BloodTiger,
      LeadedGlass,
      Basilisk,
      Nightmare,
      Decimator,
      AtomicAlloy,
      PlayerTwo,
      ChanticosFire,
      Cyrex,
      MechaIndustries,
      HyperBeast,
      Guardian,
      BrightWater,
      GoldenCoil,
      DarkWater,
      Printstream,
      // M4A4

    // Sniper
      // SG553
      // AWP
      // G3SG1
      // SCAR20
      // SSG08

      Unknown(String),
}

impl Display for WeaponSkin {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match &self {
            // Knife

            // Pistol
            // CZ75Auto
            // DesertEagle
            // DualBerettas
            // FiveSeveN
            // Glock18
            // P2000
            // P250
            // R8Revolver
            // Tec9
            // USPS

            // Shotgun
            // MAG7
            // Nova
            // SawedOff
            // XM1014

            // Heavy
            // M249
            // Negev

            // MP
            // MAC10
            // MP5SD
            // MP7
            // MP9
            // P90
            // PPBizon
            // UMP45

            // Rifle
            // AK47
            // AUG
            // FAMAS
            // GalilAR
            // M4A1S
            Self::Flashback => "Flashback",
            Self::NightTerror => "Night Terror",
            Self::EmphorosaurS => "Emphorosaur-S",
            Self::Briefing => "Briefing",
            Self::BloodTiger => "BloodTiger",
            Self::LeadedGlass => "Leaded Glass",
            Self::Basilisk => "Basilisk",
            Self::Nightmare => "Nightmare",
            Self::Decimator => "Decimator",
            Self::AtomicAlloy => "Atomic Alloy",
            Self::PlayerTwo => "Player Two",
            Self::ChanticosFire => "Chantico's Fire",
            Self::Cyrex => "Cyrex",
            Self::MechaIndustries => "Mecha Industries",
            Self::HyperBeast => "Hyper Beast",
            Self::Guardian => "Guardian",
            Self::BrightWater => "Bright Water",
            Self::GoldenCoil => "Golden Coil",
            Self::DarkWater => "Dark Water",
            Self::Printstream => "Printstream",
            // M4A4

            // Sniper
            // SG553
            // AWP
            // G3SG1
            // SCAR20
            // SSG08

            Self::Unknown(name) => name,
        })
    }
}

impl WeaponSkin {
    pub fn try_from_str(raw: &str) -> Result<Self, Box<dyn Error>> {
        let cleaned = raw.chars().filter(|c| !c.is_whitespace()).collect::<String>()
            .replace('-', "")
            .to_lowercase();

        match cleaned.as_ref() {
            // Knife

            // Pistol
            // CZ75Auto
            // DesertEagle
            // DualBerettas
            // FiveSeveN
            // Glock18
            // P2000
            // P250
            // R8Revolver
            // Tec9
            // USPS

            // Shotgun
            // MAG7
            // Nova
            // SawedOff
            // XM1014

            // Heavy
            // M249
            // Negev

            // MP
            // MAC10
            // MP5SD
            // MP7
            // MP9
            // P90
            // PPBizon
            // UMP45

            // Rifle
            // AK47
            // AUG
            // FAMAS
            // GalilAR
            // M4A1S
            "flashback" => Ok(Self::Flashback),
            "nightterror" => Ok(Self::NightTerror),
            "emphorosaur" => Ok(Self::EmphorosaurS),
            "emphorosaurs" => Ok(Self::EmphorosaurS),
            "briefing" => Ok(Self::Briefing),
            "bloodtiger" => Ok(Self::BloodTiger),
            "leadedglass" => Ok(Self::LeadedGlass),
            "basilisk" => Ok(Self::Basilisk),
            "nightmare" => Ok(Self::Nightmare),
            "decimator" => Ok(Self::Decimator),
            "atomicalloy" => Ok(Self::AtomicAlloy),
            "playertwo" => Ok(Self::PlayerTwo),
            "chanticosfire" => Ok(Self::ChanticosFire),
            "chanticofire" => Ok(Self::ChanticosFire),
            "cyrex" => Ok(Self::Cyrex),
            "mechaindustries" => Ok(Self::MechaIndustries),
            "mechaindustry" => Ok(Self::MechaIndustries),
            "hyperbeast" => Ok(Self::HyperBeast),
            "guardian" => Ok(Self::Guardian),
            "brightwater" => Ok(Self::BrightWater),
            "goldencoil" => Ok(Self::GoldenCoil),
            "darkwater" => Ok(Self::DarkWater),
            "printstream" => Ok(Self::Printstream),
            // M4A4

            // Sniper
            // SG553
            // AWP
            // G3SG1
            // SCAR20
            // SSG08

            // _ => Err(format!("Could not parse weapon skin name \"{}\"", raw).into()),
            _ => Ok(Self::Unknown(raw.to_string())),
        }
    }
}

impl FromStr for WeaponSkin {
    // TODO: Don't use String as error type
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match Self::try_from_str(s) {
            Ok(value) => Ok(value),
            Err(error) => Err(error.to_string()),
        }
    }
}
