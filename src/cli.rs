use argh::FromArgs;

use crate::{weapon_search::{WeaponQuality, WeaponName}, weapon_skin::WeaponSkin};

#[derive(FromArgs, Debug)]
#[argh(description = "\x1B[1;4mCS:GO Market Nametag Finder\x1B[0m")]
pub struct CliCommand {
    #[argh(subcommand)]
    pub command: Command,
}

#[derive(FromArgs, Debug)]
#[argh(subcommand)]
pub enum Command {
    Search(SearchCommand),
    RandomSearch(RandomSearchCommand),
    Loop(LoopCommand),

    ListKnownSkins(ListKnownSkinsCommand),
}

#[derive(FromArgs, Debug)]
#[argh(subcommand, name = "search")]
/// Performs a single search with provided parameters
pub struct SearchCommand {
    #[argh(positional)]
    pub weapon: WeaponName,

    #[argh(option)]
    /// quality
    pub quality: Option<WeaponQuality>,

    #[argh(switch)]
    /// search for listings with StatTrak™
    pub stat_trak: bool,

    #[argh(option)]
    /// what skin to search for
    pub skin: Option<WeaponSkin>,
}

#[derive(FromArgs, Debug)]
#[argh(subcommand, name = "random-search")]
/// Performs a single search with a random weapon from the search config
pub struct RandomSearchCommand {}

#[derive(FromArgs, Debug)]
#[argh(subcommand, name = "loop")]
/// Performs searches in an infinite loop with random weapons from the search config
pub struct LoopCommand {}

#[derive(FromArgs, Debug)]
#[argh(subcommand, name = "list-known-skins")]
/// Lists known skins for all weapons or for a single one if specified
pub struct ListKnownSkinsCommand {
    #[argh(option)]
    /// list only known skins for the given weapon
    pub weapon: Option<WeaponName>
}
