use std::{error::Error, process::exit, thread::sleep, time::Duration};

use cli::{CliCommand, LoopCommand, RandomSearchCommand, SearchCommand, ListKnownSkinsCommand};
use parser::{MarketResponse, Asset};
use rand::Rng;
use reqwest::blocking::Client;
use weapon_search::{WeaponSearch, SearchConfig, WeaponQuality};
use weapon_skin::{random_skin_for_weapon, get_known_skins_for_weapon};

use crate::{weapon_search::load_search_config, weapon_skin::WeaponSkin};

mod parser;
mod weapon_search;
mod weapon_skin;
mod cli;

fn main() {
    let command: CliCommand = argh::from_env();

    match run(command) {
        Ok(_) => exit(0),
        Err(error) => eprintln!("Error: {error}"),
    }
}

fn run(command: CliCommand) -> Result<(), Box<dyn Error>> {
    Ok(match command.command {
        cli::Command::Search(cmd) => run_search(cmd),
        cli::Command::RandomSearch(cmd) => run_random_search(cmd),
        cli::Command::Loop(cmd) => run_loop(cmd),

        cli::Command::ListKnownSkins(cmd) => Ok(run_list_known_skins(cmd)),
    }?)
}

fn run_search(command: SearchCommand) -> Result<(), Box<dyn Error>> {
    let search_config = load_search_config()?;

    let weapon_search = WeaponSearch::new(
        command.weapon,
        // TODO: Don't use to_string once WeaponSearch can deal with WeaponSkin type
        command.skin.unwrap_or_else(|| random_skin_for_weapon(&command.weapon)).to_string(),
        // TODO: Don't do that fallback-hackery when there's a function to
        command.quality.unwrap_or_else(|| WeaponQuality::random()),
        command.stat_trak,
        None,
    );

    search_nametag_for_weapon(&search_config, &weapon_search)?;

    Ok(())
}

fn run_random_search(_command: RandomSearchCommand) -> Result<(), Box<dyn Error>> {
    let search_config = load_search_config()?;

    // println!("{:#?}", search_config);
    let active_weapons = search_config.active_weapons();
    if active_weapons.is_empty() {
        return Err("No weapons configured for search".into());
    }

    let index = rand::thread_rng().gen_range(0..active_weapons.len());
    let weapon = active_weapons[index];
    search_nametag_for_weapon(&search_config, weapon)?;

    Ok(())
}

fn run_loop(_command: LoopCommand) -> Result<(), Box<dyn Error>> {
    let search_config = load_search_config()?;

    // println!("{:#?}", search_config);
    let active_weapons = search_config.active_weapons();
    if active_weapons.is_empty() {
        return Err("No weapons configured for search".into());
    }

    let search_item_count = active_weapons.len();
    println!("Loaded search config with {search_item_count} items");

    let sleep_duration_error = Duration::from_secs_f32(600.);
    let sleep_duration_regular = Duration::from_secs_f32(300.);
    let mut rng = rand::thread_rng();
    loop {
        let chosen_index = rng.gen_range(0..search_item_count);
        let weapon = &active_weapons[chosen_index];

        println!("");
        // println!("{chosen_index}");

        if let Err(error) = search_nametag_for_weapon(&search_config, weapon) {
            eprintln!("{error}");
            sleep(sleep_duration_error);
        }

        sleep(sleep_duration_regular);
    }
}

fn run_list_known_skins(command: ListKnownSkinsCommand) {
    if let Some(weapon) = &command.weapon {
        let skins = get_known_skins_for_weapon(weapon);
        for skin in skins {
            println!("{skin}");
        }

        return;
    }

    println!("TODO");
}

fn search_nametag_for_weapon(search_config: &SearchConfig, weapon: &WeaponSearch) -> Result<(), Box<dyn Error>> {
    // TODO: Print warning once WeaponSearch can deal with WeaponSkin type
    // if let WeaponSkin::Unknown(name) = &weapon.skin() {
    //     println!("Warning, skin name \"{}\" does not represent a known skin. Searching regardless.", name);
    // }

    let data = fetch_and_parse(search_config, weapon)?;
    let assets_with_nametag = find_assets_with_nametags(data.assets());

    // println!("{:#?}", assets_with_nametag);

    println!(
        "Found {} assets with nametags ({} assets searched)\n",
         assets_with_nametag.len(),
         data.assets().len(),
    );

    for asset in assets_with_nametag {
        match data.listinginfo().get(asset.id()) {
            None => {
                eprintln!("Asset {} has no listing", asset.id());
                continue;
            },
            Some(listing) => {
                println!(
                    "Asset with ID {} has nametag: {} | {:.2} EUR",
                    asset.id(),
                    asset.get_nametag().unwrap(),
                    listing.calculate_final_price(),
                );
            },
        }
    }

    Ok(())
}

fn find_assets_with_nametags(assets: &[Asset]) -> Vec<&Asset> {
    assets.iter().filter(|asset| asset.has_nametag()).collect()
}

fn fetch_and_parse(search_config: &SearchConfig, weapon: &WeaponSearch) -> Result<MarketResponse, Box<dyn Error>> {
    println!("Fetching market for {}", weapon.to_decorated_print_name());

    #[cfg(debug_assertions)]
    {
        if std::path::Path::new("./response.json").exists() {
            println!("Found response.json, using testing data");
            let data = std::fs::read_to_string("./response.json")?;

            return parser::parse_response(&data);
        }
    }

    let page = 1;
    let page_size = 40;

    let mut url = reqwest::Url::parse("https://steamcommunity.com")?;
    url.set_path(&format!(
        "/market/listings/730/{}/render/",
        weapon.to_search_name()
            // The Steam Client encodes these chars, but it works without doing so too
            // .replace('|', "%7C")
            // .replace('(', "%28")
            // .replace(')', "%29")
            // .replace('\'', "%27")
        ,
    ));

    url.set_query(Some(&format!(
        "query={}&start={}&count={}&country=DE&language=german&currency=3&norender=1&sort_column={}&sort_dir={}",
        weapon.search_term_as_string(),
        (page - 1) * page_size,
        page_size,
        search_config.sort_field(),
        search_config.sort_direction().as_str(),
    )));

    let response = Client::builder().build()?
        .get(url)
        .header("Accept", "application/json")
        // .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; Valve Steam Client/default/1679113038) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36")
        .send()?;

    // The Steam servers accept 200 request in five minutes or on average one request every 1.5 seconds.
    // This should be enough, however I've run into rate limiting evn by using the regular market inside the Steam Client
    // Maybe a page size of 100 counts as 10 requests (because default page size is 10)?
    if response.status() != 200 {
        return Err(format!(
            "Unexpected HTTP StatusCode: {}",
            response.status(),
        ).into());
    }

    let raw_data = response.text().unwrap();
    // println!("{}", raw_data);

    match serde_json::from_str(&raw_data) {
        Ok(parsed) => Ok(parsed),
        Err(error) => {
            eprintln!("Writing response to error-response.json");
            std::fs::write("error-response.json", raw_data)?;
            Err(error.into())
        },
    }

}
