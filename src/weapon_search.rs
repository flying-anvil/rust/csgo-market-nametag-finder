use std::{error::Error, fs, str::FromStr};

use rand::Rng;
use serde::{Serialize, Deserialize, Deserializer, de::Unexpected};
use serde_aux::prelude::bool_true;

pub fn load_search_config() -> Result<SearchConfig, Box<dyn Error>> {
    match fs::read_to_string("./data/search_config.json") {
        Err(_) => Err("Could not load search config (./data/search_config.json)".into()),
        Ok(contents) => Ok(serde_json::from_str(&contents)?),
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SearchConfig {
    // #[serde(default)]
    // currency: u16, // Is currently unused
    #[serde(default = "default_sort_field")]
    sort_field: String,
    #[serde(default)]
    sort_direction: SortDirection,
    #[serde(default)]
    weapons: Vec<WeaponSearch>,
}

impl SearchConfig {
    pub fn sort_field(&self) -> &str {&self.sort_field}
    pub fn sort_direction(&self) -> SortDirection {self.sort_direction}
    #[allow(dead_code)]
    pub fn weapons(&self) -> &Vec<WeaponSearch> {&self.weapons}
    pub fn active_weapons(&self) -> Vec<&WeaponSearch> {self.weapons.iter().filter(|weapon| weapon.active()).collect()}
}

fn default_sort_field() -> String {
    "price".to_string()
}

#[derive(Default, Clone, Copy, Debug)]
pub enum SortDirection {
    #[default]
    Ascending,
    Descending,
}

impl SortDirection {
    pub fn from_str(raw: &str) -> Result<Self, Box<dyn Error>> {
        let cleaned = raw.to_lowercase();

        match cleaned.as_ref() {
            "ascending" => Ok(Self::Ascending),
            "asc" => Ok(Self::Ascending),
            "a" => Ok(Self::Ascending),

            "descending" => Ok(Self::Descending),
            "desc" => Ok(Self::Descending),
            "d" => Ok(Self::Descending),

            _ => Err(format!("Could not parse direction \"{}\"", raw).into()),
        }
    }

    pub fn as_str(&self) -> &str {
        match self {
            SortDirection::Ascending => "asc",
            SortDirection::Descending => "desc",
        }
    }
}

impl<'de> Deserialize<'de> for SortDirection {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de> {
        let string = String::deserialize(deserializer)?;
        match SortDirection::from_str(&string) {
            Ok(value) => Ok(value),
            Err(_) => Err(serde::de::Error::invalid_value(
                Unexpected::Str(&string),
                &"ascending/asc/a or descending/desc/d",
            )),
        }
    }
}

impl Serialize for SortDirection {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer {
        str::serialize(self.as_str(), serializer)
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct WeaponSearch {
    weapon: WeaponName,     // TODO: Make optional, random if omitted
    skin: String,           // TODO: Make optional, random if omitted
    quality: WeaponQuality, // TODO: Make optional, random if omitted
    #[serde(default = "bool_true")]
    stat_track: bool,
    search_term: Option<String>,
    #[serde(default = "bool_true")]
    active: bool,
}

#[allow(dead_code)]
impl WeaponSearch {
    pub fn new(weapon: WeaponName, skin: String, quality: WeaponQuality, stat_track: bool, search_term: Option<String>) -> Self { Self { weapon, skin, quality, stat_track, search_term, active: true } }

    pub fn weapon(&self) -> WeaponName {self.weapon}
    pub fn skin(&self) -> &str {&self.skin}
    pub fn quality(&self) -> WeaponQuality {self.quality}
    pub fn stat_track(&self) -> bool {self.stat_track}
    pub fn active(&self) -> bool {self.active}

    pub fn search_term_as_string(&self) -> &str {
        match &self.search_term {
            Some(value) => value,
            None => "",
        }
    }

    pub fn to_search_name(&self) -> String {
        // "StatTrak™ M4A1-S | Night Terror (Battle-Scarred)"
        format!(
            "{}{} | {} ({})",
            if self.stat_track {"StatTrak™ "} else {""},
            self.weapon.as_str(),
            self.skin,
            self.quality.as_str(),
        )
    }

    pub fn to_decorated_print_name(&self) -> String {
        format!(
            // Bold + Color
            "\x1B[1;38;2;{}m{}\x1B[0m",
            if self.stat_track { "207;106;50" } else { "210;210;210" },
            self.to_search_name(),
        )
    }
}

#[derive(Clone, Copy, Debug)]
#[allow(clippy::upper_case_acronyms)]
pub enum WeaponName {
    Knife,

    // Pistol
    CZ75Auto,
    DesertEagle,
    DualBerettas,
    FiveSeveN,
    Glock18,
    P2000,
    P250,
    R8Revolver,
    Tec9,
    USPS,

    // Shotgun
    MAG7,
    Nova,
    SawedOff,
    XM1014,

    // Heavy
    M249,
    Negev,

    // MP
    MAC10,
    MP5SD,
    MP7,
    MP9,
    P90,
    PPBizon,
    UMP45,

    // Rifle
    AK47,
    AUG,
    FAMAS,
    GalilAR,
    M4A1S,
    M4A4,

    // Sniper
    SG553,
    AWP,
    G3SG1,
    SCAR20,
    SSG08,
}

impl WeaponName {
    pub fn try_from_str(raw: &str) -> Result<Self, Box<dyn Error>> {
        let cleaned = raw.chars().filter(|c| !c.is_whitespace()).collect::<String>()
            .replace('-', "")
            .to_lowercase();

        match cleaned.as_ref() {
            "knife" => Ok(Self::Knife),

            // ====================

            "cz75auto" => Ok(Self::CZ75Auto),

            "deserteagle" => Ok(Self::DesertEagle),

            "dualberettas" => Ok(Self::DualBerettas),
            "akimboberettas" => Ok(Self::DualBerettas),
            "berettas" => Ok(Self::DualBerettas),

            "fiveseven" => Ok(Self::FiveSeveN),
            "57" => Ok(Self::FiveSeveN),

            "glock18" => Ok(Self::Glock18),
            "glock" => Ok(Self::Glock18),

            "p2000" => Ok(Self::P2000),

            "p250" => Ok(Self::P250),

            "r8revolver" => Ok(Self::R8Revolver),
            "r8" => Ok(Self::R8Revolver),
            "revolver" => Ok(Self::R8Revolver),

            "tec9" => Ok(Self::Tec9),
            "tec" => Ok(Self::Tec9),

            "upss" => Ok(Self::USPS),
            "usp" => Ok(Self::USPS),

            // ====================

            "mag7" => Ok(Self::MAG7),
            "mag" => Ok(Self::MAG7),

            "nova" => Ok(Self::Nova),
            "snipershotgun" => Ok(Self::Nova),

            "sawedoff" => Ok(Self::SawedOff),
            "sawed" => Ok(Self::SawedOff),

            "xm1014" => Ok(Self::XM1014),
            "1014" => Ok(Self::XM1014),
            "xm" => Ok(Self::XM1014),

            // ====================

            "m249" => Ok(Self::M249),
            "249" => Ok(Self::M249),
            "m" => Ok(Self::M249),

            "negev" => Ok(Self::Negev),

            // ====================

            "mac10" => Ok(Self::MAC10),
            "mac" => Ok(Self::MAC10),

            "mp5sd" => Ok(Self::MP5SD),
            "mp5" => Ok(Self::MP5SD),

            "mp7" => Ok(Self::MP7),

            "mp9" => Ok(Self::MP9),

            "p90" => Ok(Self::P90),

            "ppbizon" => Ok(Self::PPBizon),
            "bizon" => Ok(Self::PPBizon),

            "ump45" => Ok(Self::UMP45),
            "ump" => Ok(Self::UMP45),

            // ====================

            "ak47" => Ok(Self::AK47),
            "47" => Ok(Self::AK47),
            "ak" => Ok(Self::AK47),

            "aug" => Ok(Self::AUG),

            "famas" => Ok(Self::FAMAS),

            "galilar" => Ok(Self::GalilAR),
            "galil" => Ok(Self::GalilAR),

            "m4a1s" => Ok(Self::M4A1S),
            "m4s" => Ok(Self::M4A1S),

            "m4a4" => Ok(Self::M4A4),
            "m4" => Ok(Self::M4A4),

            "sg553" => Ok(Self::SG553),
            "sg" => Ok(Self::SG553),

            // ====================

            "awp" => Ok(Self::AWP),

            "g3sg1" => Ok(Self::G3SG1),

            "scar20" => Ok(Self::SCAR20),
            "scar" => Ok(Self::SCAR20),

            "ssg08" => Ok(Self::SSG08),
            "ssg" => Ok(Self::SSG08),

            _ => Err(format!("Could not parse weapon name \"{}\"", raw).into()),
        }
    }

    pub fn as_str(&self) -> &str {
        match self {
            WeaponName::Knife => "Knife",

            WeaponName::CZ75Auto => "CZ75-Auto",
            WeaponName::DesertEagle => "Desert Eagle",
            WeaponName::DualBerettas => "Dual Berettas",
            WeaponName::FiveSeveN => "Five-SeveN",
            WeaponName::Glock18 => "Glock-18",
            WeaponName::P2000 => "P2000",
            WeaponName::P250 => "P250",
            WeaponName::R8Revolver => "R8 Revolver",
            WeaponName::Tec9 => "Tec-9",
            WeaponName::USPS => "USP-S",

            WeaponName::MAG7 => "MAG-7",
            WeaponName::Nova => "Nova",
            WeaponName::SawedOff => "Sawed-Off",
            WeaponName::XM1014 => "XM1014",

            WeaponName::M249 => "M249",
            WeaponName::Negev => "Negev",

            WeaponName::MAC10 => "MAC-10",
            WeaponName::MP5SD => "MP5-SD",
            WeaponName::MP7 => "MP7",
            WeaponName::MP9 => "MP9",
            WeaponName::P90 => "P90",
            WeaponName::PPBizon => "PP-Bizon",
            WeaponName::UMP45 => "UMP-45",

            WeaponName::AK47 => "AK-47",
            WeaponName::AUG => "AUG",
            WeaponName::FAMAS => "FAMAS",
            WeaponName::GalilAR => "Galil AR",
            WeaponName::M4A1S => "M4A1-S",
            WeaponName::M4A4 => "M4A4",
            WeaponName::SG553 => "SG 553",

            WeaponName::AWP => "AWP",
            WeaponName::G3SG1 => "G3SG1",
            WeaponName::SCAR20 => "SCAR-20",
            WeaponName::SSG08 => "SSG 08",
        }
    }
}

impl<'de> Deserialize<'de> for WeaponName {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de> {
        let string = String::deserialize(deserializer)?;
        match WeaponName::try_from_str(&string) {
            Ok(value) => Ok(value),
            Err(_) => Err(serde::de::Error::invalid_value(
                Unexpected::Str(&string),
                &"existing weapon name",
            )),
        }
    }
}

impl Serialize for WeaponName {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer {
        str::serialize(self.as_str(), serializer)
    }
}

impl FromStr for WeaponName {
    // TODO: Don't use String as error type
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match Self::try_from_str(s) {
            Ok(value) => Ok(value),
            Err(error) => Err(error.to_string()),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum WeaponQuality {
    FactoryNew,
    MinimalWear,
    FiedTested,
    WellWorn,
    BattleScarred,
}

impl WeaponQuality {
    pub fn try_from_str(raw: &str) -> Result<Self, Box<dyn Error>> {
        let cleaned = raw.chars().filter(|c| !c.is_whitespace()).collect::<String>()
            .replace('-', "")
            .to_lowercase();

        match cleaned.as_ref() {
            "factorynew" => Ok(Self::FactoryNew),
            "factory" => Ok(Self::FactoryNew),
            "new" => Ok(Self::FactoryNew),
            "fn" => Ok(Self::FactoryNew),

            "minimalwear" => Ok(WeaponQuality::MinimalWear),
            "minimal" => Ok(WeaponQuality::MinimalWear),
            "wear" => Ok(WeaponQuality::MinimalWear),
            "mw" => Ok(WeaponQuality::MinimalWear),

            "fieldtested" => Ok(WeaponQuality::FiedTested),
            "field" => Ok(WeaponQuality::FiedTested),
            "tested" => Ok(WeaponQuality::FiedTested),
            "ft" => Ok(WeaponQuality::FiedTested),

            "wellworn" => Ok(WeaponQuality::WellWorn),
            "worn" => Ok(WeaponQuality::WellWorn),
            "ww" => Ok(WeaponQuality::WellWorn),

            "battlescarred" => Ok(WeaponQuality::BattleScarred),
            "battle" => Ok(WeaponQuality::BattleScarred),
            "scarred" => Ok(WeaponQuality::BattleScarred),
            "bs" => Ok(WeaponQuality::BattleScarred),

            _ => Err(format!("Could not parse quality name \"{}\"", raw).into()),
        }
    }

    pub fn as_str(&self) -> &str {
        match self {
            WeaponQuality::FactoryNew => "Factory New",
            WeaponQuality::MinimalWear => "Minimal Wear",
            WeaponQuality::FiedTested => "Field-Tested",
            WeaponQuality::WellWorn => "Well-Worn",
            WeaponQuality::BattleScarred => "Battle-Scarred",
        }
    }

    pub fn random() -> Self {
        let number = rand::thread_rng().gen_range(0..5);
        match number {
            0 => WeaponQuality::FactoryNew,
            1 => WeaponQuality::MinimalWear,
            2 => WeaponQuality::FiedTested,
            3 => WeaponQuality::WellWorn,
            4 => WeaponQuality::BattleScarred,
            _ => panic!("Unexpected number for random quality: {number}"),
        }
    }
}

impl FromStr for WeaponQuality {
    // TODO: Don't use String as error type
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match Self::try_from_str(s) {
            Ok(value) => Ok(value),
            Err(error) => Err(error.to_string()),
        }
    }
}

impl<'de> Deserialize<'de> for WeaponQuality {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de> {
        let string = String::deserialize(deserializer)?;
        match WeaponQuality::try_from_str(&string) {
            Ok(value) => Ok(value),
            Err(_) => Err(serde::de::Error::invalid_value(
                Unexpected::Str(&string),
                &"existing quality name",
            )),
        }
    }
}

impl Serialize for WeaponQuality {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer {
        str::serialize(self.as_str(), serializer)
    }
}
